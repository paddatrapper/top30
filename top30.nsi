!include MUI2.nsh

;--------------------------------

Name "Top30"
OutFile "dist/top30.exe"
InstallDir $PROGRAMFILES\Top30
LicenseData LICENCE.md

;--------------------------------

; Pages

!insertmacro MUI_PAGE_LICENSE LICENCE.md
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------

; Languages

!insertmacro MUI_LANGUAGE "English"

;--------------------------------

Section "Top30 (required)"

  SectionIn RO

  SetOutPath $INSTDIR
  File /r dist\top30\*

  WriteRegStr HKLM SOFTWARE\Top30 "Install_Dir" "$INSTDIR"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Top30" "DisplayName" "NSIS Top30"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Top30" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Top30" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Top30" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

SectionEnd

Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Top30"
  CreateShortcut "$SMPROGRAMS\Top30\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortcut "$SMPROGRAMS\Top30\Top30.lnk" "$INSTDIR\top30.exe" "" "$INSTDIR\top30.exe" 0
  
SectionEnd

Section "Desktop Shortcut"

  CreateShortcut "$DESKTOP\Top30.lnk" "$INSTDIR\top30.exe" "" "$INSTDIR\top30.exe" 0
  
SectionEnd

Section "Uninstall"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Top30"
  DeleteRegKey HKLM SOFTWARE\NSIS_Top30

  Delete /REBOOTOK $INSTDIR\uninstall.exe
  Delete "$SMPROGRAMS\Top30\*.*"
  Delete $DESKTOP\Top30.lnk

  RMDir "$SMPROGRAMS\Top30"
  RMDir /r /REBOOTOK "$INSTDIR"

SectionEnd