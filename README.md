# top30

Automatically creates run downs of songs given certain parameters.

## Building

On any platform with Python 3, the following will build and install the latest
release of top30:

    pip install top30

Installing from source is done as follows

    python3 setup.py install

### Development Install

When developing Top30, it is useful to use a virtual environment to avoid
poluting your desktop with unnecessary libraries. This is done using
[VirtualEnv](https://virtualenv.pypa.io/en/stable/).

Once VirtualEnv is installed, fork, clone the repo and create a virtual
environment:

    git clone https://gitlab.com/paddatrapper/top30
    cd top30
    virtualenv -p python3 pyenv
    pyenv/bin/pip install -e .

Then the project can be edited and finally run as follows:

    pyenv/bin/top30


### Windows Executable and Installer

A Windows executable can be created using PyInstaller. This creates the
directory `dist/top30`, which then can be distributed run. It requires the
ffmpeg static build to be downloaded into `../ffmpeg`

    pip install pyinstaller
    pyinstaller top30.spec

Once the executable has been created, an installer can be generated using
[NSIS](http://nsis.sourceforge.net/Download/):

    makensis top30.nsi

## Running

top30 can be run as either a command-line application or a GUI. The CLI version
takes two arguments - the current chart file and the previous chart file. Both
are expected to be in the [format](#Chart Format) provided by UCT Radio

    rundown-creator -c some_chart.txt -p some_chart.txt

The GUI has no such restrictions and can be run as is:

    top30

## GUI Operation

The GUI allows files to be added in sequence to the list on the left. These
files are then parsed into a rundown. All files with a readable start time in
the description field of the metadata (can be changed in the configuration file
to be any metadata field) are assumed to be songs, while any others are assumed
to be voice tracks. Songs are clipped to the length specified under Song Length
and are overlapped with voice tracks using the timings specified. All times are
in seconds.

## Chart Format
All charts are expected to be in plain text. The content is expected to follow
the following structure:

```
**Position** | **Artist** | **Song** | **Reg** | **Last Week**
1        | Example    | Song     | Loc     | Up 5
```
